#!/bin/bash
# If PYNQ is connected via Ethernet to a Linux PC, this will allow the PYNQ board to resolve URLs

echo "Adding DNS servers 8.8.8.8 and 8.8.4.4 to resolv.conf"
sudo echo "nameserver 8.8.8.8" >> /etc/resolv.conf
sudo echo "nameserver 8.8.4.4" >> /etc/resolv.conf 
echo "NOTE: this needs to run at every power cycle"

echo "TODO: MAKE THIS PERMANENT!!!!!!!!!!!!"
