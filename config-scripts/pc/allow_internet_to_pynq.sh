#!/bin/bash
# This scripts allows the Wifi from the PC running to to be shared through eth0, allowing any device connected, and that has been properly configured, to use the internet.
# Run this if PYNQ is connected directly to a Linux PC via Ethernet
iptables-restore < /etc/iptables.ipv4.nat
