# Engine Sensor Interface

Engine Sensor Interface - 2019 Research Assistant Work

## Direcotry Structure
```
├── config-scripts      # Contains optional configuration scripts, read comments in them to find out more
├── docs                # Contains useful documentation
├── web-app             # Top-level web application
├──── py-scripts        # All Python3 scripts used for setting up the PYNQ and server
├──── webpage           # Webpage contents
└────── resources       # Resources, JavaScript, images, etc, used by the webpage
└── README.md
```

## File Dependencies
### Server
The server code is all done with Python.  It uses a Tornado Web Server application.  The 3 main files are the app.py (top-level application calling server.py), server.py (the actual server, it also creates an instance of PynqBrd), and the pynq_brd.py (lower level board functionality).  server.py receives and sends messages from and to the client.  server.py is the code that "talks" to the client "ws-client.js" script.
### Client
The client code is written in both HTML (for the webpage) using Bootstrap 4 and JavaScript (for the client functionality see ws-client.js).  The index.html file is what the user will see when they arrive at the webpage.  All webpage functionalities are done in JavaScript by the client code, which send a message to the server and interpret a message from the server.
### Other Dependencies
All code has been downloaded to the PYNQ board's SD card.  The board does not need internet access as it is all located under web-app/webpage/resources
## Note
Please download the repo and save it onto your own repo.  I will not grant write permission.