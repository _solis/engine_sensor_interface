#!/usr/bin/env python3
'''
This script recieves a signal from PMOD AD2 and displays the
data on the webserver
'''
# For FPGA
from pynq.overlays.base import BaseOverlay
from pynq.lib import Pmod_ADC
# For plots
import matplotlib
matplotlib.use('agg')
import numpy as np
import matplotlib.pyplot as plt
import datetime
import csv
import os
'''
This class wraps the functionality needed from the PYNQ board
__init__ takes about 7 seconds
read_adc takes about 2 ms
'''
PMODA = 0
PMODB = 1
class PynqBrd:
    def __init__(self, enA, enB, cnt):
        # Load the FPGA base bitstream
        self.base = BaseOverlay("base.bit")
        
        # Holds whether or not user wants these enabled
        self.enableA = enA
        self.enableB = enB
        
        # Get the PMOD interface to use
        self.adcA = Pmod_ADC(self.base.PMODA)
        self.adcB = Pmod_ADC(self.base.PMODA)
        
        # Holds the current data as string
        self.csvfile = "log_" + str(cnt) + "_" + str(datetime.date.today()) + ".csv"
        # store some parameters
        if(enA == "true" or enB == "true"):
            csvfile = open(self.csvfile, 'w')
            with csvfile as csv_file:
                writer = csv.writer(csv_file)
                writer.writerows([["", "", "", "", "", "", "", "", "", ""]])
                writer.writerows([["PMOD Interface", "PMODA", "", "", "", "PMODB", "", "", "", ""]])
                writer.writerows([["","Timestamp","ADC_CH1","ADC_CH2","ADC_CH3","Timestamp","ADC_CH1","ADC_CH2","ADC_CH3"]])
            csvfile.close()
    '''
    Description:
        Read single value for both ADC channels
    '''
    def read_adc(self):
        # Get voltage samples
        self.stamp = "Last updated: " + str(datetime.datetime.utcnow().isoformat())
        # always read from both PMODS, the read function returns the VOLTAGE for that specific channel
        # REMEMBER, this is only a voltage from 0 - 2.048V for now
        valuesA = self.adcA.read(ch1=True, ch2=True, ch3=True)
        valuesB = self.adcB.read(ch1=True, ch2=True, ch3=True)
        
# TODO: Actual equations in format(...)
# Some math needs to be performed for the voltage values read
# For instance, if 1V represents 1000 RPM on channel 0 PMODA, then valuesA[0]*1000
        # convert ch1 pmod A
        self.ch01_A = "{0:.2f}".format(valuesA[0])
        # convert ch2
        self.ch02_A = "{0:.2f}".format(valuesA[1])
        # convert ch3
        self.ch03_A = "{0:.2f}".format(valuesA[2])
        # convert ch1 pmod B
        self.ch01_B = "{0:.2f}".format(valuesB[0])
        # convert ch2
        self.ch02_B = "{0:.2f}".format(valuesB[1])
        # convert ch3
        self.ch03_B = "{0:.2f}".format(valuesB[2])

        # Save values to CSV
        if(self.enableA == "true" or self.enableB == "true"):
            csvfile = open(self.csvfile, 'a')
            with csvfile as csv_file:
                writer = csv.writer(csv_file)
                writer.writerows([["",self.stamp, self.ch01_A, self.ch02_A, self.ch03_A, self.stamp, self.ch01_B, self.ch02_B, self.ch03_B]])
            csvfile.close()
    '''
    Description:
        Distructor
    '''
    def __del__(self):
        self.adcA.reset()
        self.adcB.reset()
        del self.adcA,self.adcB,self.base
