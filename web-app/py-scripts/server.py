#!/usr/bin/env python3
'''
This script recieves a signal from PMOD AD2 and displays the
data and plots it using matplotlib
'''
# For server
import os
import os.path
from zipfile import ZipFile
import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import datetime
import time
from pynq_brd import *
import base64

''' TORNADO FRAMEWORK STUFF '''
# This is the actual application
# open, close, and on_message are required by the tornado webserver
class TaskHandler(tornado.websocket.WebSocketHandler):
    def initialize(self):
        self.progress  = 0
        self.pmoda_cfg = "false"
        self.pmodb_cfg = "false"
        self.pynq      = None
        self.run       = 0
        self.initCount = 0

    # update progress bar
    def update_progress(self, amt):
        self.progress += amt
        self.write_message(str(self.progress)+"%")
    
    def init_board(self):
        self.pynq = PynqBrd(self.pmoda_cfg, self.pmoda_cfg, self.initCount)
        self.initCount = self.initCount + 1

    # this function is continously called after "Run" in the web browswer, every 2ms
    def poll_adc(self):
        self.pynq.read_adc()
        time.sleep(0.001)
        if(self.pmoda_cfg == "true"):
            self.write_message("A_"+self.pynq.stamp)
            self.write_message(self.pynq.ch01_A+"ch01_A")
            self.write_message(self.pynq.ch02_A+"ch02_A")
            #self.write_message(self.pynq.ch03_A+"ch03_A")
        if(self.pmodb_cfg == "true"):
            self.write_message("B_"+self.pynq.stamp)
            self.write_message(self.pynq.ch01_B+"ch01_B")
            self.write_message(self.pynq.ch02_B+"ch02_B")
            #self.write_message(self.pynq.ch03_B+"ch03_B")
    
    # this gets called whenever the server opens a connection with the browser
    def open(self):
        print("[WS]: Connection was opened.")
        message = "open"
    
    # this is called whenever a message from the browser comes to the server
    # it is interpreted here
    def on_message(self, message):
        # CHECK INCOMING MESSAGE FROM WEBPAGE FOR KEYWORDS
        # READYTOCONFIG, [pmodA enabled?], [pmodB enabled?]
        # the above is the structure of the message for configuration
        if("READYTOCONFIG" in message):
            self.progress = 0
            self.update_progress(0)
            print("[INFO]: initializing board")
            # Get the message and parse it
            msglist = message.split(",")
            self.pmoda_cfg = msglist[1]
            self.pmodb_cfg = msglist[2]

            self.update_progress(10)
            self.init_board()
            # haven't figured out a way to independently update the progress bar
            while(self.progress != 80):
                self.update_progress(10)
                time.sleep(0.5)
            self.progress = 100
            self.update_progress(0)
            print("[INFO]: initializing done")       
                 
        elif("START" in message):
            self.poll_adc()

        elif("DOWNLOAD" in message):
            # grab all the csvs in logs/
            filelist = [ f for f in os.listdir(os.getcwd()) if f.endswith(".csv") ]
            # Add all CSVs to zip
            with ZipFile("data.zip",'w') as zip:
                for f in filelist:
                    zip.write(f)
            # delete the CSVs to save memory
            for f in filelist:
                os.remove(f)
            # encode the zip file as base64 for client side and save in ws-client local dir
            path_to_zip64 = "../webpage/resources/js/data.zip.b64"
            with open('data.zip', 'rb') as fin, open(path_to_zip64, 'wb') as fout:
                base64.encode(fin, fout)
            # delete the actual zip
            os.remove("data.zip")
            # initiate client download by sending it a message
            self.write_message("DOWNLOADNOW")

    # called whenever the connection between server and client closes
    def on_close(self):
        print("[WS]: Connection was closed.")
        message = "close"

# this is the main handler, it renders the webpage and calls the task handler
# this is needed by the tornado webserver
class MainHandler(tornado.web.RequestHandler):
    def get(self):
        print("[MainHandler]: User Connected.")
        self.render("index.html")
# application class used by app.py
class App:
    def __init__(self):
        # these are basically,
        # template_path: location of index.html
        # static_path: location of all resources such as images, javascript, etc.
        self.settings = dict(
	        template_path = os.path.join(os.path.dirname(__file__), "../webpage"),
	        static_path   = os.path.join(os.path.dirname(__file__), "../webpage/resources")
	    )
        # this is the actual call to create the tornado applicaiton
        self.app = tornado.web.Application([
                (r'/', MainHandler),
                (r'/ws', TaskHandler),
            ], **self.settings)
