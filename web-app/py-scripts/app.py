#! /usr/bin/python3
print("[INFO]: server will start shortly...")
from server import *
'''
PARAMETERS
'''
# Tonado server port - this can be changed, but make sure to type the correct one in your browser
PORT = 88
'''
    MAIN
'''
if __name__ == "__main__":
    # Start server and get pmod and frequencies to configure the board
    app = App()
     
    try:
        http_server = tornado.httpserver.HTTPServer(app.app)
        http_server.listen(PORT)
        main_loop = tornado.ioloop.IOLoop.instance()

        print ("PYNQ Server started")
        main_loop.start()

    except Exception as e:
        print(e)
        print("Exception triggered - PYNQ Server stopped.")
