/*
This constructs the actual client - how the browser will act
*/
var callBackID = false;
$(document).ready(function()
{
    // disable all buttons first
    $(':input[type="submit"]').prop('disabled', false);
    $(':input[type="button"]').prop('disabled', true);
   
    // start client code
    var WEBSOCKET_ROUTE = "/ws";
    if(window.location.protocol == "http:")
    {
        // localhost
        var ws = new WebSocket("ws://" + window.location.host + WEBSOCKET_ROUTE);
        console.log("[INFO]: WebSocket as localhost (ws://)");
    }
    else if(window.location.protocol == "https:")
    {
        // Dataplicity
        var ws = new WebSocket("wss://" + window.location.host + WEBSOCKET_ROUTE);
        console.log("[INFO]: WebSocket as dataplicity (wss://)");
    }
    else
    {
        console.log("[ERROR]: Starting WebSocket");
    }

    // gets the server/client status - seen at the bottom of the webpage
    ws.onopen = function(evt) 
    {
        document.getElementById("ws-status").innerHTML = "Connected";
    };
    // intepret the messages recieved by the server basically, the message 
    // gets checked for specific keywoards that are interpreted by if statements
    ws.onmessage = function(evt) 
    {
        var message = evt.data;
        // % refers to progress bar percentage
        if(message.includes("%"))
        {
            // change bar accordingly
            $("#bar")
            .css("width", message)
            .attr("aria-valuenow", message)
            .text(message);
            // enable the start button only after we've reached 100% initialization
            if (message !== "100%")
            {
              $('.start-btn').prop("disabled",true);
            }
            else
            {
              $('.start-btn').prop("disabled",false);
            }
            // download button is always disabled until we have already run
            $('.down-btn').prop("disabled",true);
        }
        // Check for all PMODA channels
        // NOTE: NO CHANNEL 3, BUT CAN BE ADDED
        else if(message.includes("ch01_A"))
        {
            // remove unused text
            var angle = message.replace("ch01_A","");
            // replace the webpage with our value
            document.getElementById("ch01-Atxt").innerHTML = angle;
        }
        else if(message.includes("ch02_A"))
        {
            var rpm = message.replace("ch02_A","");
            document.getElementById("ch02-Atxt").innerHTML = rpm;
        }
        // get our timestamp and update
        else if(message.includes("A_Last updated:"))
        {
            var stamp = message.replace("A_","");
            document.getElementById("stamp-txtA01").innerHTML = stamp;
            document.getElementById("stamp-txtA02").innerHTML = stamp;
        }
        // get PMODB's channels and timestamp
        else if(message.includes("ch01_B"))
        {
            var angle = message.replace("ch01_B","");
            document.getElementById("ch01-Btxt").innerHTML = angle;
        }
        else if(message.includes("ch02_B"))
        {
            var rpm = message.replace("ch02_B","");
            document.getElementById("ch02-Btxt").innerHTML = rpm;
        }
        else if(message.includes("B_Last updated:"))
        {
            var stamp = message.replace("B_","");
            document.getElementById("stamp-txtB01").innerHTML = stamp;
            document.getElementById("stamp-txtB02").innerHTML = stamp;
        }
        // if server tells us that we can download the zip file
        else if(message.includes("DOWNLOADNOW"))
        {
            var zip_file_path = window.location.href;
            var zip_file_name = "data.zip.b64";
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            a.href = zip_file_path;
            a.download = zip_file_name;
            a.click();
            document.body.removeChild(a);
        }
    };
    // executed when we close the connection
    ws.onclose = function(evt) 
    {
        document.getElementById("ws-status").innerHTML = "Disconnected";
    };
    
    // Check form, not really needed but added it anyways
    var form = document.querySelector('.needs-validation');
    form.addEventListener('submit', function(event) 
    {
        event.preventDefault();
        var check = form.checkValidity();
        if (check === false) 
        {
            event.preventDefault();
            event.stopPropagation();
        }
        // get the checkboxes to see which PMOD interface is enabled
        var enable_pmodA    = document.getElementById("enable-pmodA").checked;
        var enable_pmodB    = document.getElementById("enable-pmodB").checked;
        // button disable/enable logic
        $('.start-btn').prop("disabled",true);
        $('.stop-btn').prop("disabled",true);
        $('.down-btn').prop("disabled",true);
        // if we are currently running, end it
        if(callBackID)
        {
            clearInterval(callBackID);
        }
        // create the message that will configure the board
        var pmod_cfg = "READYTOCONFIG," + enable_pmodA + "," + enable_pmodB;

        // send it to debug and server
        console.log(pmod_cfg);
        ws.send(pmod_cfg);
        
        // more button logic
        $('.start-btn').prop("disabled",false);
        $('.down-btn').prop("disabled",false);
        form.classList.add('was-validated');
    })
    // CONTROL BUTTON FUNCTIONS
    $('.start-btn').on('click',function() 
    {
        $('.stop-btn').prop("disabled",false);
        $('.start-btn').prop("disabled",true);
        $('.down-btn').prop("disabled",false);
        // this is where we actually continously (every 5ms) ask to poll the ADC
        callBackID = setInterval(function(){ ws.send("START"); }, 5);
        console.log("START");
        ws.send("START");
    });
    $('.stop-btn').on('click',function() 
    {
        clearInterval(callBackID);
        callBackID = false;
        $('.stop-btn').prop("disabled",true);
        $('.start-btn').prop("disabled",false);
        $('.down-btn').prop("disabled",false);
        console.log("STOP");
        ws.send("STOP");
    });
    $('.down-btn').on('click',function() 
    {
        if(callBackID)
        {
            clearInterval(callBackID);
            callBackID = false;
        }
        $('.stop-btn').prop("disabled",true);
        $('.start-btn').prop("disabled",false);
        $('.down-btn').prop("disabled",true);
        console.log("DOWNLOAD");
        ws.send("DOWNLOAD");
    });
});
